#include "CommandShell.h"
#include "Computer_Memory.h"
#include<fstream> // allows access to files
#include <iostream>

std::string CommandShell::IntakeCommandFromUser()
{
	std::cout << ">"; // text
	std::string userCommand; // makes a string
	getline(std::cin, userCommand); // gets line and makes the users input userCommand
	ProcessCommand(userCommand); // gets command Input
	return userCommand; // returns the users input
}

void CommandShell::ProcessCommand(std::string commandToProcess)
{
	if (commandToProcess == "help" || commandToProcess == "Help")
	{
		Help(); // displays help
	}
	else if (commandToProcess == "displaymem" || commandToProcess == "DM" || commandToProcess == "DisplayMem")
	{
		DisplayMemory(); // displays memory
	}
	else if (commandToProcess == "load" || commandToProcess == "Load")
	{
		Load(); // loads from a file
	}
	else if (commandToProcess == "run" || commandToProcess == "Run")
	{
		Run(); // runs
	}
	else if (commandToProcess == "exit" || commandToProcess == "Exit")
	{
		// exits
	}
	else
	{
		system("CLS"); // clears the screen
		std::cout << "UNKNOWN COMMAND\n"; // text
	}
}

void CommandShell::Help()
{
	system("CLS"); // clears the screen
	std::cout << "THIS IS THE HELP MENU\n\n"; // text
	std::cout << "Help - Displays this Help Menu\n"; // text
	std::cout << "Load - Loads a program into memory\n"; // text
	std::cout << "Run - Runs the program in memory\n"; // text
	std::cout << "DisplayMem/DM - Displays all memory locations and values aka Dump memory\n"; // text
	std::cout << "Exit - Exits this Shell\n"; // text
}

void CommandShell::Load()
{
	system("CLS"); // clears the screen
	
	try // attempts to run the following code
	{
		std::string myText; // place holder variable

		std::ifstream MyReadFile("Memory.txt"); // the file to load

		int address = 0; // where to start loading from
		while (std::getline(MyReadFile, myText)) 
		{
			SetRAM_Value(address++, myText); // updates the memory
		}

	}
	catch (...) // if an error occurs the code here runs
	{
		std::cout << "You really broke something to get here... \n"; // lets the user know that they should not be here
	}
}

void CommandShell::Run()
{
	system("CLS"); // clears the screen
	std::cout << "Running the program...\n"; // text
	// todo: need Killians code...
}

void CommandShell::DisplayMemory()
{
	for (int i = 0; i < 100; i++)
	{
		std::cout << "\n Address = " << i << " " << "Value = " << GetRAM(i); // displays the memory
	}
	std::cout << "\n"; // text
}