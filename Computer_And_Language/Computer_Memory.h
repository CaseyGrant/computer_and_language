#pragma once
#include <string>

class Computer_Memory
{
public:
	void SetRAM_Value(int address, std::string value); // updates the memory
	std::string GetRAM(int address); // gets the memory
private:
	// 0-99
	std::string RAM[100]; // holds the memory
};