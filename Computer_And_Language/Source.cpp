#include <iostream>
#include<string>

#include "CommandShell.h"

void main()
{
	std::cout << "LINK START!\n"; // text
	CommandShell command_shell; // makes a command shell 
	std::cout << "Start By Typing a Command\n"; // text

	do
	{
		command_shell.IntakeCommandFromUser(); // makes userCommand something that can be taken in in command shell source file
	} while (command_shell.IntakeCommandFromUser() != "exit"); // checks if the user input exit

	std::cout << "Thank you Goodbye now"; // text
}