#pragma once
#include <string>
#include "Computer_Memory.h"

class CommandShell : Computer_Memory
{
public:
	std::string IntakeCommandFromUser(); // gets user input

private:
	void ProcessCommand(std::string commandToProcess); // makes ProcessCommand
	void Help(); // displays help
	void DisplayMemory(); // displays the memory
	void Load(); // loads from a file
	void Run(); // makes the code run
};