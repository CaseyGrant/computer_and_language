#include "Computer_Memory.h"

void Computer_Memory::SetRAM_Value(int address, std::string value)
{
	RAM[address] = value; // updates the ram at an address
}

std::string Computer_Memory::GetRAM(int address)
{
	return RAM[address]; // returns the address in the ram
}
